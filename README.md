# leetcode-hub-mysql

基于 `mysql8.0.x` + `mysql-test-run` 的 leetcode 练习仓库。

- `t` 存放 `mysql-test-run` 所需的 `.test` 文件。
- `r` 存放 `mysql-test-run` 所需的 `.result` 文件。

## 环境信息

```sh
$ uname -a
Linux admin0-VirtualBox 5.13.0-28-generic #31~20.04.1-Ubuntu SMP Wed Jan 19 14:08:10 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux

$ ./bin/mysqltest --version
./bin/mysqltest  Ver 8.0.28 for Linux on x86_64 (MySQL Community Server - GPL)
```

## 使用方法

项目运行依赖 `mysql-test-run` 环境（windows 环境搭建较复杂，建议基于 linux 环境搭建）。

`mysql-test-run` 官方文档: [https://dev.mysql.com/doc/dev/mysql-server/latest/PAGE_MYSQL_TEST_RUN.html](https://dev.mysql.com/doc/dev/mysql-server/latest/PAGE_MYSQL_TEST_RUN.html)

普通的二进制包是没有 `mysql-test` 的，需要额外下载 `Test Suite`，下载链接: [https://dev.mysql.com/downloads/mysql/](https://dev.mysql.com/downloads/mysql/)

| Compressed TAR Archive                            | MD5                              |
| ------------------------------------------------- | -------------------------------- |
| `mysql-8.0.28-linux-glibc2.12-x86_64.tar.xz`      | 5be32f68d6859aace1eb61cea1d00bff |
| `mysql-test-8.0.28-linux-glibc2.12-x86_64.tar.xz` | 1aa16282acb18eb7cc74ea024989058b |

```sh
# 解压压缩包
$ tar xvf mysql-8.0.28-linux-glibc2.12-x86_64.tar.xz
$ tar xvf mysql-test-8.0.28-linux-glibc2.12-x86_64.tar.xz

# clone 本项目到 /mysql-test/suite 目录
$ cd mysql-8.0.28-linux-glibc2.12-x86_64/mysql-test/suite/
$ git clone https://gitee.com/gdut_yy/leetcode-hub-mysql.git
$ cd ..

# 运行 mysql-test-run
$ ./mtr --suite leetcode-hub-mysql
```

（全文完）
